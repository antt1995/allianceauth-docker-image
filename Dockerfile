FROM python:3.8
ADD . /app
RUN pip install -e /app gunicorn && \
    allianceauth start myauth
WORKDIR /myauth
ENTRYPOINT [ "/app/docker/entrypoint.sh" ]
CMD ["gunicorn","--bind","0.0.0.0:8000","myauth.wsgi"]
