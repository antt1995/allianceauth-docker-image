#!/usr/bin/env bash
set -e

# Wair for database to come up
/app/docker/wait-for-it.sh -h mariadb -p 3306 -t 120

# Initialize if needed
if [ ! -f /FLAG_INITIALIZED ] && [ -z ${NO_INIT+x} ]; then
	python /myauth/manage.py migrate
	mkdir -p /var/www/myauth/static
	python /myauth/manage.py collectstatic --noinput
	python /myauth/manage.py check
fi

if [ -f /FLAG_INITIALIZED ] && [ "$@" = "migrate" ]; then
	 python /myauth/manage.py migrate --noinput
fi

if [ -f /FLAG_INITIALIZED ] && [ "$@" = "update" ]; then
	allianceauth update /myauth
fi

# Execute passed command
exec "$@"
